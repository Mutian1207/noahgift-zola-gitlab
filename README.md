# IDS 721 Mini Project 1

## Requirements
* Static site generated with Zola
* Home page and portfolio project page templates
Styled with CSS
* GitLab repo with source code

## Run

1. Clone this repo to your local
```
git clone 
```

2. Use zola to build the website
``` 
zola build && zola serve --open
```

## Result
`style the website with css`:

![](https://gitlab.com/Mutian1207/noahgift-zola-gitlab/-/raw/main/css.jpg?ref_type=heads&inline=false)

This part is to set my picture's size and shape.

`Home page:`
![](https://gitlab.com/Mutian1207/noahgift-zola-gitlab/-/raw/main/res1.jpg?ref_type=heads&inline=false)

`Portfolio project page:`
![](https://gitlab.com/Mutian1207/noahgift-zola-gitlab/-/raw/main/res2.jpg?ref_type=heads&inline=false)
