+++
title = "Final Project 👋"
description = "Introducing AdiDoks, a Zola theme helping you build modern documentation websites, which is a port of the Hugo theme Doks for Zola."
date = 2021-04-03T07:00:00+00:00
updated = 2021-04-03T07:00:00+00:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["Mutian Ling"]

[extra]
lead = 'LLMOps - Model Serving with Rust'
images = []
+++
