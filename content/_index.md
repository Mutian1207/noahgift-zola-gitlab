+++
# title = "Modern Documentation Theme"


# The homepage contents
[extra]
lead ='Mutian Ling, M.S. @Duke ECE'

repo_url = "https://github.com/aaranxu/adidoks"

# Menu items
# [[extra.menu.main]]
# name = "Docs"
# section = "docs"
# url = "/docs/getting-started/introduction/"
# weight = 10

[[extra.menu.main]]
name = "project"
section = "project"
url = "/project/"
weight = 20


[[extra.list]]
title = "Education:"
content = "MS, Duke University, 2024 <br> BEc, Soochow University, 2022"


[[extra.list]]
title = "Work Experience"
content = 'None'

[[extra.list]]
title = "Skills"
content = "C/C++, Python, Go, Rust, Linux, AWS, Docker, Kubernetes"

# [[extra.list]]
# title = "Full text search"
# content = "Search your Doks site with FlexSearch. Easily customize index settings and search options to your liking."

# [[extra.list]]
# title = "Page layouts"
# content = "Build pages with a landing page, blog, or documentation layout. Add custom sections and components to suit your needs."


+++
